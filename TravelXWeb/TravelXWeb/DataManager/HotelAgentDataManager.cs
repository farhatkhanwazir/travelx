﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using TravelXWeb.TravelXDataBaseModel;
using TravelX.ViewModel;

namespace TravelXWeb.DataManager
{
    public static class HotelAgentDataManager
    {
        #region Member Variables

        static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion
        public static HotelAgentCreateViewModel GetHotelAgentCreateViewModel()
        {
            using (TravelXEntities db = new TravelXEntities())
            {
                HotelAgentCreateViewModel vm = new HotelAgentCreateViewModel()
                {
                    HotelDropDownViewModel = db.Hotels.ToList().Select(h => h.CreateFromHotel()),
                    PriceUnitDropDownViewModel = db.PriceUnits.ToList().Select(pr => pr.CreateFromPriceUnit()),
                    RoomTypeDropDownViewModel = db.RoomTypes.ToList().Select(rt => rt.CreateFromRoomType()),
                    AgentDropDownViewModel = db.AspNetUsers.ToList().Select(usr => usr.CreateFromUser())
                };
                return vm;
            }
        }

        public static HotelAgentEditViewModel GetHotelAgentForEdit(int hotelAgentId)
        {
            using (TravelXEntities db = new TravelXEntities())
            {
                HotelAgentEditViewModel vm = new HotelAgentEditViewModel()
                {
                    HotelDropDownViewModel = db.Hotels.ToList().Select(h => h.CreateFromHotel()),
                    PriceUnitDropDownViewModel = db.PriceUnits.ToList().Select(pr => pr.CreateFromPriceUnit()),
                    RoomTypeDropDownViewModel = db.RoomTypes.ToList().Select(rt => rt.CreateFromRoomType()),
                    AgentDropDownViewModel = db.AspNetUsers.ToList().Select(usr => usr.CreateFromUser())
                };
                var hotelAgentUser = db.HotelAgentRooms.FirstOrDefault(agent => agent.hotel_agent_room_id == hotelAgentId);
                if (hotelAgentUser != null)
                {
                    vm.HotelAgentRoomId = hotelAgentUser.hotel_agent_room_id;
                    vm.HotelId = hotelAgentUser.hotel_id;
                    vm.RoomId = hotelAgentUser.room_id;
                    vm.NoOfRooms = hotelAgentUser.no_of_rooms;
                    vm.AgentId = hotelAgentUser.agent_id; //viewModel.AgentId,
                    vm.PriceUnitId = hotelAgentUser.price_unit_id;
                    vm.AgentPrice = hotelAgentUser.agent_price;
                    vm.CoustomerPrice = hotelAgentUser.coustomer_price;
                    vm.IsWeekendPrice = hotelAgentUser.is_weekend_price??false;
                    vm.WeekendPagentPrice = hotelAgentUser.weekend_pagent_price;
                    vm.WeekendCoustomerPrice = hotelAgentUser.weekend_coustomer_price;
                    vm.EffectiveFrom = hotelAgentUser.effective_from;
                    vm.EffectiveTo = hotelAgentUser.effective_to;
                    vm.IsActive = hotelAgentUser.is_active;
                    vm.IsDeleted = hotelAgentUser.is_deleted;
                    vm.IsVerified = hotelAgentUser.is_verified;
                    vm.CreatedBy = hotelAgentUser.created_by;
                    vm.CreatedDate = hotelAgentUser.created_date;
                    vm.UpdatedBy = hotelAgentUser.updated_by;
                    vm.UpdatedDate = hotelAgentUser.updated_date;
                }
                return vm;
            }
        }
        public static SuccessErrorModel CreateHotelAgent(HotelAgentCreateViewModel viewModel)
        {
            var hotelAgent = ConvertHotelAgentVmToHotelAgent(viewModel);
            try
            {
                using (TravelXEntities db = new TravelXEntities())
                {
                    db.HotelAgentRooms.Add(hotelAgent);
                    db.SaveChanges();
                    return new SuccessErrorModel(true, "Created");
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error saving Hotel Agent", ex);
                return new SuccessErrorModel(false, "Error saving Hotel Agent", ex);
            }
        }

        public static SuccessErrorModel HotelAgentEdit(HotelAgentEditViewModel viewModel)
        {
            var hotelAgent = ConvertHotelAgentEditVmToHotelAgent(viewModel);
            try
            {
                using (TravelXEntities db = new TravelXEntities())
                {
                    db.HotelAgentRooms.AddOrUpdate(hotelAgent);
                    db.SaveChanges();
                    return new SuccessErrorModel(true, "Edited");
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error Editing Hotel Agent", ex);
                return new SuccessErrorModel(false, "Error Editing Hotel Agent", ex);
            }
        }

        public static SuccessErrorModel HotelAgentDelete(int id)
        {
            try
            {
                using (TravelXEntities db = new TravelXEntities())
                {
                    var hotelAgent = db.HotelAgentRooms.FirstOrDefault(agent => agent.hotel_agent_room_id == id);
                    if (hotelAgent != null)
                    {
                        hotelAgent.is_deleted = true;
                        hotelAgent.is_active = false;
                        db.HotelAgentRooms.AddOrUpdate(hotelAgent);
                        db.SaveChanges();
                    }
                    return new SuccessErrorModel(true, "Deleted");
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error Deleting Hotel Agent", ex);
                return new SuccessErrorModel(false, "Error Deleting HotelAgent", ex);
            }
        }

        public static HotelAgentViewModel IndexHotelAgent()
        {
            using (TravelXEntities db = new TravelXEntities())
            {
                return HotelAgentToIndexView(db.HotelAgentRooms.Where(ha=>ha.is_active==true && ha.is_deleted==false));
            }
        }

        private static HotelAgentViewModel HotelAgentToIndexView(IEnumerable<HotelAgentRoom> viewModel)
        {
            HotelAgentViewModel vm = new HotelAgentViewModel();
            foreach (HotelAgentRoom hotelAgent in viewModel)
            {
                vm.HotelIndexViewModel.Add(ConvertHotelAgentToViewModel(hotelAgent));
            }
            return vm;
        }

        private static HotelAgentIndexVIewModel ConvertHotelAgentToViewModel(HotelAgentRoom hotelAgent)
        {
            return new HotelAgentIndexVIewModel()
            {
                HotelAgentId = hotelAgent.hotel_agent_room_id,
                HotelName = hotelAgent.Hotel.hotel_name ?? "",
                HotelAgentName = "Hotel Agent Name", //replace with aspnetuser name who create this,                
                RoomType = hotelAgent.RoomType.type_name ?? "",
                AgentPrice = hotelAgent.agent_price ?? 0.00,
                CustomerPrice = hotelAgent.coustomer_price ?? 0.00,
                FromDate = hotelAgent.effective_from.ToString("yyyy-MM-dd HH:mm:ss"),
                ToDate = hotelAgent.effective_to?.ToString("yyyy-MM-dd HH:mm:ss") ?? "<not available>"
        };
    }
    private static HotelDropDownViewModel CreateFromHotel(this Hotel hotel)
    {
        return new HotelDropDownViewModel()
        {
            HotelId = hotel.hotel_id,
            HotelName = hotel.hotel_name
        };
    }

    private static PriceUnitDropDownViewModel CreateFromPriceUnit(this PriceUnit priceUnit)
    {
        return new PriceUnitDropDownViewModel()
        {
            PriceUnitId = priceUnit.price_unit_id,
            PriceUnitName = priceUnit.price_unit_name
        };
    }

    private static RoomTypeDropDownViewModel CreateFromRoomType(this RoomType roomType)
    {
        return new RoomTypeDropDownViewModel()
        {
            RoomId = roomType.room_id,
            RoomType = roomType.type_name
        };
    }

    private static AgentDropDownViewModel CreateFromUser(this AspNetUser user)
    {
        return new AgentDropDownViewModel()
        {
            AgentId = user.Id,
            AgentName = user.FirstName + " " + user.LastName
        };
    }

    public static HotelAgentRoom ConvertHotelAgentVmToHotelAgent(this HotelAgentCreateViewModel viewModel)
    {
        return new HotelAgentRoom()
        {
            hotel_id = viewModel.HotelId,
            room_id = viewModel.RoomId,
            no_of_rooms = viewModel.NoOfRooms,
            agent_id = viewModel.AgentId, //viewModel.AgentId,
            price_unit_id = viewModel.PriceUnitId,
            agent_price = viewModel.AgentPrice,
            coustomer_price = viewModel.CoustomerPrice,
            is_weekend_price = viewModel.IsWeekendPrice,
            weekend_pagent_price = viewModel.WeekendPagentPrice,
            weekend_coustomer_price = viewModel.WeekendCoustomerPrice,
            effective_from = viewModel.EffectiveFrom ?? DateTime.Now,
            effective_to = viewModel.EffectiveTo,
            is_active = viewModel.IsActive,
            is_deleted = viewModel.IsDeleted,
            is_verified = viewModel.IsVerified,
            created_by = (int)(System.Web.HttpContext.Current.Session["UserId"]),
            created_date = System.DateTime.Now,
            updated_by = null,
            updated_date = null
        };
    }
        public static HotelAgentRoom ConvertHotelAgentEditVmToHotelAgent(this HotelAgentEditViewModel viewModel)
        {
            return new HotelAgentRoom()
            {
                hotel_agent_room_id = viewModel.HotelAgentRoomId,
                hotel_id = viewModel.HotelId,
                room_id = viewModel.RoomId,
                no_of_rooms = viewModel.NoOfRooms,
                agent_id = viewModel.AgentId,
                price_unit_id = viewModel.PriceUnitId,
                agent_price = viewModel.AgentPrice,
                coustomer_price = viewModel.CoustomerPrice,
                is_weekend_price = viewModel.IsWeekendPrice,
                weekend_pagent_price = viewModel.WeekendPagentPrice,
                weekend_coustomer_price = viewModel.WeekendCoustomerPrice,
                effective_from = viewModel.EffectiveFrom ?? DateTime.Now,
                effective_to = viewModel.EffectiveTo,
                is_active = viewModel.IsActive,
                is_deleted = viewModel.IsDeleted,
                is_verified = viewModel.IsVerified,
                created_by = viewModel.CreatedBy,
                created_date = viewModel.CreatedDate,
                updated_by = (int)(System.Web.HttpContext.Current.Session["UserId"]),
                updated_date = System.DateTime.Now
            };
        }
    }   
}