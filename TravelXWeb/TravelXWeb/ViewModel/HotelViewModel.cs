﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TravelXWeb.ViewModel
{
    public class HotelViewModel
    {
    }

    public class HotelCreateViewModel
    {
        public int StarId { get; set; }
        public IEnumerable<SelectListItem> HotelStarList { get; set; }
        public IEnumerable<HttpPostedFileBase> FileList { get; set; }
        public IEnumerable<CityDropDownViewModel> CityDropDownList { get; set; }
        public HotelCreateViewModel()
        {
            CityDropDownList = new List<CityDropDownViewModel>();
            HotelStarList = new List<SelectListItem>();
            FileList=new List<HttpPostedFileBase>();
        }

        public int HotelId { get; set; }
        public string HotelName { get; set; }
        public int? CityId { get; set; }
        public int Stars { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public string Fax { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double? DistanceFromMainRoad { get; set; }
        public double? DistanceFromHaram { get; set; }
        public bool IsMeal { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsVerified { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public HttpPostedFileBase Files { get; set; }
    }

    public class CityDropDownViewModel
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
    }

    public enum HotelStar
    {
        OneStar = 1,
        TwoStar = 2,
        ThreeStar = 3,
        FourStar = 4,
        FiveStar = 5,
        SixStar = 6,
        SevenStar = 7,
    }

    //public class HotelStars
    //{
    //    public int StarId { get; set; }
    //    public string StarName { get; set; }
    //}

    public class HotelIndexViewModel
    {
        public int HotelId { get; set; }
        public string HotelName { get; set; }
        public string HotelAdress { get; set; }
        public int HotelStar { get; set; }
        public string HotelImage { get; set; }

    }

    public class HotelIndexViewModelList
    {
        public List<HotelIndexViewModel> HotelIndexViewList { get; set; }

        public HotelIndexViewModelList()
        {
            HotelIndexViewList = new List<HotelIndexViewModel>();
        }
    }

    public class HotelIndexEditViweModel
    {
        public int StarId { get; set; }
        public IEnumerable<HttpPostedFileBase> FileList { get; set; }
        public IEnumerable<CityDropDownViewModel> CityDropDownList { get; set; }
        public IEnumerable<SelectListItem> HotelStarList { get; set; }
        public IEnumerable<HotelImageListForEdit> HotelImagelistForEdit { get; set; }
        public HotelIndexEditViweModel()
        {
            CityDropDownList = new List<CityDropDownViewModel>();
            HotelStarList = new List<SelectListItem>();
            FileList = new List<HttpPostedFileBase>();
            HotelImagelistForEdit=new List<HotelImageListForEdit>();
        }

        public int HotelId { get; set; }
        public string HotelName { get; set; }
        public int? CityId { get; set; }
        public int Stars { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public string Fax { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double? DistanceFromMainRoad { get; set; }
        public double? DistanceFromHaram { get; set; }
        public bool IsMeal { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsVerified { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public HttpPostedFileBase Files { get; set; }
    }

    public class HotelImageListForEdit
    {
        public int HotelImageId { get; set; }
        public string ImagePath { get; set; }
    }
}