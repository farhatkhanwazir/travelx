﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TravelXWeb.TravelXDataBaseModel;

namespace TravelXWeb.ViewModel
{
    public class UserListViewModel
    {
        public List<UserViewModel> UserList { get; set; }

        public UserListViewModel()
        {
            UserList = new List<UserViewModel>();
        }
    }

    public class UserViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public string UserName { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }
        public string Phone { get; set; }

      //  public UserViewModel() { }
    }
    public class UserCreateViewModel
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string Address { get; set; }

        [Required]
        //[DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [Required]
        public string Password { get; set; }

        [Display]
        public List<AspNetRole> systemRole { get; set; }
        public int RoleId { get; set; }
        public UserCreateViewModel()
        {
            systemRole= new List<AspNetRole>();
        }
    }

    public class UserUpdateViewModel
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string Address { get; set; }

        [Required]
        //[DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        //[Required]
        public string Password { get; set; }
        public string SecurityStamp { get; set; }
        public bool LockoutEnabled { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }

        [Display]
        public List<AspNetRole> systemRole { get; set; }
        public int RoleId { get; set; }
        public UserUpdateViewModel()
        {
            systemRole = new List<AspNetRole>();
        }
    }

    public class UserLoginViewModel
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class UserProfile
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNO { get; set; }
    }

    public class UserSetting
    {
        public string OldPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "NewPassword")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Confirmation Password is required.")]
        [Compare("NewPassword", ErrorMessage = "Password and Confirmation Password must match.")]
        public string ConfirmNewPassword { get; set; }

        public string OldEmail { get; set; }

        public string  NewEmail { get; set; }
        [Required(ErrorMessage = "Confirmation Email is required.")]
        [Compare("NewEmail", ErrorMessage = "NewEmail and Confirmation Email must match.")]
        public string ConfirmEmail { get; set; }
    }
}