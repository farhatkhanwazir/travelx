﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TravelXWeb.DataManager;
using TravelXWeb.TravelXDataBaseModel;
using TravelX.ViewModel;

namespace TravelXWeb.Controllers
{
    [Authorize]
    public class RoomController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: Room
        public ActionResult Index()
        {
            HotelAgentViewModel vm = new HotelAgentViewModel();
            vm = HotelAgentDataManager.IndexHotelAgent();
            return View(vm);
        }

        // GET: Room/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Room/Create
        public ActionResult Create()
        {
            try
            {
                HotelAgentCreateViewModel viewModel = new HotelAgentCreateViewModel();
                viewModel = HotelAgentDataManager.GetHotelAgentCreateViewModel();
                return View(viewModel);
            }
            catch (Exception ex)
            {
                logger.Error("User Create Page Load", ex);
                return View("Error");
            }
        }

        // POST: Room/Create
        [HttpPost]
        public ActionResult Create(HotelAgentCreateViewModel viewModel)
        {
            // TODO: Add insert logic here
            var result = HotelAgentDataManager.CreateHotelAgent(viewModel);
            if (result.IsSuccessful)
            {
                return RedirectToAction("Index");
            }
            else
            {
                //return ModelState.AddModelError("HotelAgent", result.ExceptionObject != null ? result.ExceptionObject.ToString() : result.ErrorMessage);
                return View("Error");
            }
        }

        // GET: Room/Edit/5
        public ActionResult Edit(int id)
        {
            HotelAgentEditViewModel vm= new HotelAgentEditViewModel();
            vm = HotelAgentDataManager.GetHotelAgentForEdit(id);
            return View(vm);
        }

        // POST: Room/Edit/5
        [HttpPost]
        public ActionResult Edit(HotelAgentEditViewModel viewModel)
        {
            // TODO: Add insert logic here
            var result = HotelAgentDataManager.HotelAgentEdit(viewModel);
            if (result.IsSuccessful)
            {
                return RedirectToAction("Index");
            }
            else
            {
                //return ModelState.AddModelError("HotelAgent", result.ExceptionObject != null ? result.ExceptionObject.ToString() : result.ErrorMessage);
                return View("Error");
            }
        }

        // GET: Room/Delete/5
        public ActionResult Delete(int id)
        {
            var result = HotelAgentDataManager.HotelAgentDelete(id);
            if (result.IsSuccessful)
            {
                return RedirectToAction("Index");
            }
            else
            {
                //return ModelState.AddModelError("HotelAgent", result.ExceptionObject != null ? result.ExceptionObject.ToString() : result.ErrorMessage);
                return View("Error");
            }
        }

        // POST: Room/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
