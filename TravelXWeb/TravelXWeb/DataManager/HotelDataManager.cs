﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using TravelXWeb.TravelXDataBaseModel;
using TravelX.ViewModel;
using TravelXWeb.ViewModel;

namespace TravelXWeb.DataManager
{
    public static class HotelDataManager
    {
        #region Member Variables

        static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        public static HotelCreateViewModel GetHotelCreateViewModel()
        {
            using (TravelXEntities db = new TravelXEntities())
            {
                IEnumerable<HotelStar> actionTypes = Enum.GetValues(typeof(HotelStar))
                                                       .Cast<HotelStar>();
                HotelCreateViewModel vm = new HotelCreateViewModel()
                {
                    HotelStarList = from action in actionTypes
                                    select new SelectListItem
                                    {
                                        Text = action.ToString(),
                                        Value = ((int)action).ToString()
                                    },
                    CityDropDownList = db.Cities.ToList().Select(c => c.CreateFromCities())
                };
                return vm;
            }
        }

        public static SuccessErrorModel HotelCreateViewModel(HotelCreateViewModel viewModel)
        {
            var hotel = ConvertHotelCreateVmToHotel(viewModel);
            int hotelId = 0;
            try
            {
                using (TravelXEntities db = new TravelXEntities())
                {
                    db.Hotels.Add(hotel);
                    db.SaveChanges();
                    var firstOrDefault = db.Hotels.FirstOrDefault(h => h.hotel_name == hotel.hotel_name && h.description == hotel.description &&
                                                                       h.mobile_no == hotel.mobile_no && h.phone_no == hotel.phone_no);
                    if (firstOrDefault != null)

                        foreach (HttpPostedFileBase file in viewModel.FileList)
                        {
                            string fileName = Path.GetFileName(file.FileName ??"");
                            {
                                string targetFolder = Path.Combine(HttpContext.Current.Server.MapPath("~/img/HotelImages/"),fileName);
                                //string targetPath = Path.Combine(fileName,targetFolder);
                                file.SaveAs(targetFolder);
                                hotelId = firstOrDefault.hotel_id;
                                var hotelImage = new HotelImage();
                                hotelImage.hotel_id = hotelId;
                                hotelImage.ImagePath = fileName;
                                hotelImage.is_default = true;
                                hotelImage.is_active = true;
                                hotelImage.is_deleted = false;
                                hotelImage.is_verified = true;
                                hotelImage.created_by = (int)(System.Web.HttpContext.Current.Session["UserId"]);
                                hotelImage.created_date = DateTime.Now;
                                hotelImage.updated_by = null;
                                hotelImage.updated_date = null;
                                db.HotelImages.Add(hotelImage);
                            }
                            db.SaveChanges();

                        }

                    //if (firstOrDefault != null)
                    //{
                    //    VarbinaryStream blob = new VarbinaryStream(
                    //        System.Configuration.ConfigurationManager.ConnectionStrings["TravelXConnection"].ConnectionString,
                    //        "HotelImages",
                    //        "Image_id",
                    //        "hotel_image_id",
                    //        hotelImage.hotel_image_id);

                    //    Debug.WriteLine("Total length: " + viewModel.ProjectInformation.InputStream.Length);
                    //    viewModel.ProjectInformation.InputStream.CopyTo(blob, 16080);
                    //}
                    return new SuccessErrorModel(true, "Created");
                }

            }
            catch (Exception ex)
            {
                logger.Error("Error saving Hotel Agent", ex);
                return new SuccessErrorModel(false, "Error saving Hotel Agent", ex);
            }
        }

        public static HotelIndexViewModelList GetAllHotelForIndexView()
        {
            using (TravelXEntities db = new TravelXEntities())
            {
                return HotelIndexViewListForView(db.Hotels.Where(hotl=>hotl.is_active==true && hotl.is_deleted==false));
            }
        }

        public static HotelIndexEditViweModel GetHotelForEdit(int id)
        {
            using (TravelXEntities db = new TravelXEntities())
            {
                IEnumerable<HotelStar> actionTypes = Enum.GetValues(typeof(HotelStar))
                    .Cast<HotelStar>();
                HotelIndexEditViweModel vm = new HotelIndexEditViweModel()
                {
                    HotelStarList = from action in actionTypes
                                    select new SelectListItem
                                    {
                                        Text = action.ToString(),
                                        Value = ((int)action).ToString()
                                    },
                    CityDropDownList = db.Cities.ToList().Select(c => c.CreateFromCities()),
                    HotelImagelistForEdit = db.HotelImages.Where(hid => hid.hotel_id == id && hid.is_active==true && hid.is_deleted==false).ToList().Select(hm => hm.CreateFromHotelImage())
                };
                var hotel = db.Hotels.FirstOrDefault(agent => agent.hotel_id == id && agent.is_active==true);
                if (hotel != null)
                {
                    vm.HotelId = hotel.hotel_id;
                    vm.HotelName = hotel.hotel_name;
                    vm.CityId = hotel.city_id;
                    vm.StarId = hotel.stars.Value;
                    vm.Description = hotel.description;
                    vm.Address = hotel.address;
                    vm.PhoneNo = hotel.phone_no;
                    vm.MobileNo = hotel.mobile_no;
                    vm.Fax = hotel.fax;
                    vm.Latitude = hotel.latitude;
                    vm.Longitude = hotel.longitude;
                    vm.DistanceFromMainRoad = hotel.distance_from_main_road;
                    vm.DistanceFromHaram = hotel.distance_from_haram;
                    vm.IsMeal = hotel.is_meal;
                    vm.IsActive = hotel.is_active;
                    vm.IsDeleted = hotel.is_deleted;
                    vm.IsVerified = hotel.is_verified;
                    vm.CreatedBy = (int) hotel.created_by;
                    vm.CreatedDate = hotel.created_date;
                    vm.UpdatedBy = (int)(System.Web.HttpContext.Current.Session["UserId"]);
                    //vm.UpdatedBy = Convert.ToInt64(System.Web.HttpContext.Current.Session["UserId"];
                    vm.UpdatedDate = DateTime.Now;
                }

                return vm;
            }
        }

        public static SuccessErrorModel HotelEdit(HotelIndexEditViweModel viewModel)
        {
            try
            {
                var hotel = ConvertHotelCreateVmToHotelForEdit(viewModel);
                //var firstOrDefault =
                //    db.Hotels.FirstOrDefault(h => h.hotel_name == hotel.hotel_name && h.description == hotel.description &&
                //                                  h.mobile_no == hotel.mobile_no && h.phone_no == hotel.phone_no);
                using (TravelXEntities db = new TravelXEntities())
                {
                    db.Hotels.AddOrUpdate(hotel);

                    foreach (HttpPostedFileBase file in viewModel.FileList)
                    {
                        if (file != null)
                        { 
                            string fileName = Path.GetFileName(file.FileName);
                        if (fileName != null)
                        {
                            string targetFolder = HttpContext.Current.Server.MapPath("~/img/HotelImages/");
                            string targetPath = Path.Combine(targetFolder, fileName);
                            file.SaveAs(targetPath);
                            var hotelImage = new HotelImage();
                            hotelImage.hotel_id = viewModel.HotelId;
                            hotelImage.ImagePath = fileName;
                            hotelImage.is_default = true;
                            hotelImage.is_active = true;
                            hotelImage.is_deleted = false;
                            hotelImage.is_verified = true;
                            hotelImage.created_by = (int)(System.Web.HttpContext.Current.Session["UserId"]);
                            hotelImage.created_date = viewModel.CreatedDate;
                            hotelImage.updated_by = (int)(System.Web.HttpContext.Current.Session["UserId"]);
                            hotelImage.updated_date = DateTime.Now;
                            db.HotelImages.Add(hotelImage);
                        }
                    }
                    db.SaveChanges();
                    }
                    return new SuccessErrorModel(true, "Edited");
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error Editing Hotel Agent", ex);
                return new SuccessErrorModel(false, "Error Editing Hotel Agent", ex);
            }
        }

        public static bool HotelImageDelete(int hotelImageId)
        {
            try
            {
                using (TravelXEntities db = new TravelXEntities())
                {
                    var hotelImage = db.HotelImages.FirstOrDefault(himg => himg.hotel_image_id == hotelImageId);
                    hotelImage.is_deleted = true;
                    hotelImage.is_active = false;
                    db.HotelImages.AddOrUpdate(hotelImage);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }            
        }
        private static HotelIndexViewModelList HotelIndexViewListForView(IEnumerable<Hotel> viewModel)
        {
            HotelIndexViewModelList vm = new HotelIndexViewModelList();
            foreach (Hotel hotel in viewModel)
            {
                vm.HotelIndexViewList.Add(ConvertFromHotelToViewModel(hotel));
            }
            return vm;
        }

        private static HotelIndexViewModel ConvertFromHotelToViewModel(Hotel hotel)
        {            
            return new HotelIndexViewModel()
            {
                HotelId = hotel.hotel_id,
                HotelStar = hotel.stars.GetValueOrDefault(),
                HotelAdress = hotel.address,
                HotelName = hotel.hotel_name,
                HotelImage = hotel.HotelImages.FirstOrDefault()?.ImagePath
            };
        }
        private static Hotel ConvertHotelCreateVmToHotel(this HotelCreateViewModel viewModel)
        {
            return new Hotel()
            {
                //hotel_id = viewModel.HotelId,
                hotel_name = viewModel.HotelName,
                city_id = viewModel.CityId,
                stars = Convert.ToByte(viewModel.StarId),
                description = viewModel.Description,
                address = viewModel.Address,
                phone_no = viewModel.PhoneNo,
                mobile_no = viewModel.MobileNo,
                fax = viewModel.Fax,
                latitude = viewModel.Latitude,
                longitude = viewModel.Longitude,
                distance_from_haram = viewModel.DistanceFromHaram,
                distance_from_main_road = viewModel.DistanceFromMainRoad,
                is_meal = viewModel.IsMeal,
                is_active = true,
                is_deleted = viewModel.IsDeleted,
                is_verified = viewModel.IsVerified,
                created_by = (int)(System.Web.HttpContext.Current.Session["UserId"]),
                created_date = DateTime.Now,
                updated_by = null,
                updated_date = null
            };
        }
        private static Hotel ConvertHotelCreateVmToHotelForEdit(this HotelIndexEditViweModel viewModel)
        {
            return new Hotel()
            {
                hotel_id = viewModel.HotelId,
                hotel_name = viewModel.HotelName,
                city_id = viewModel.CityId,
                stars = Convert.ToByte(viewModel.StarId),
                description = viewModel.Description,
                address = viewModel.Address,
                phone_no = viewModel.PhoneNo,
                mobile_no = viewModel.MobileNo,
                fax = viewModel.Fax,
                latitude = viewModel.Latitude,
                longitude = viewModel.Longitude,
                distance_from_haram = viewModel.DistanceFromHaram,
                distance_from_main_road = viewModel.DistanceFromMainRoad,
                is_meal = viewModel.IsMeal,
                is_active = viewModel.IsActive,
                is_deleted = viewModel.IsDeleted,
                is_verified = viewModel.IsVerified,
                created_by = viewModel.CreatedBy,
                created_date = viewModel.CreatedDate,
                updated_by = (int)(System.Web.HttpContext.Current.Session["UserId"]),
                updated_date = System.DateTime.Now
            };
        }
        private static CityDropDownViewModel CreateFromCities(this City city)
        {
            return new CityDropDownViewModel()
            {
                CityId = city.city_id,
                CityName = city.city_name
            };
        }

        private static HotelImageListForEdit CreateFromHotelImage(this HotelImage hotelImage)
        {
            var imagefilePath = hotelImage.ImagePath;
            var imagePath = imagefilePath.Split('/').Last();
            return new HotelImageListForEdit()
            {
                HotelImageId = hotelImage.hotel_image_id,
                ImagePath = hotelImage.ImagePath.Substring(hotelImage.ImagePath.LastIndexOf('/')+1)
            };
        }
    }
}