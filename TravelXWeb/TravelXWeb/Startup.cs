﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TravelXWeb.Startup))]
namespace TravelXWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
