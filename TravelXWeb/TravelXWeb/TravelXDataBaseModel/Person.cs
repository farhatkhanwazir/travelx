//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TravelXWeb.TravelXDataBaseModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Person
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Person()
        {
            this.Bookings = new HashSet<Booking>();
            this.PersonImages = new HashSet<PersonImage>();
        }
    
        public long person_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string caste_name { get; set; }
        public Nullable<System.DateTime> date_of_birth { get; set; }
        public string nic { get; set; }
        public string passport_no { get; set; }
        public Nullable<int> city_id { get; set; }
        public string full_address { get; set; }
        public string phone_no { get; set; }
        public string mobile_no { get; set; }
        public string email_address { get; set; }
        public Nullable<bool> is_active { get; set; }
        public Nullable<bool> is_deleted { get; set; }
        public Nullable<bool> is_verified { get; set; }
        public Nullable<int> created_by { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<int> updated_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Booking> Bookings { get; set; }
        public virtual City City { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PersonImage> PersonImages { get; set; }
    }
}
