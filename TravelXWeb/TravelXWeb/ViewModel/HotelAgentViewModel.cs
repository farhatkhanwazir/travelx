﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

namespace TravelX.ViewModel
{
    public class HotelAgentViewModel
    {
        public List<HotelAgentIndexVIewModel> HotelIndexViewModel { get; set; }

        public HotelAgentViewModel()
        {
            HotelIndexViewModel= new List<HotelAgentIndexVIewModel>();
        }
    }
    public class HotelAgentCreateViewModel
    {

        public IEnumerable<HotelDropDownViewModel> HotelDropDownViewModel { get; set; }
        public IEnumerable<PriceUnitDropDownViewModel> PriceUnitDropDownViewModel { get; set; }
        public IEnumerable<RoomTypeDropDownViewModel> RoomTypeDropDownViewModel { get; set; }
        public IEnumerable<AgentDropDownViewModel> AgentDropDownViewModel { get; set; }
        public HotelAgentCreateViewModel()
        {
            HotelDropDownViewModel = new List<HotelDropDownViewModel>();
            PriceUnitDropDownViewModel = new List<PriceUnitDropDownViewModel>();
            RoomTypeDropDownViewModel = new List<RoomTypeDropDownViewModel>();
            AgentDropDownViewModel= new List<AgentDropDownViewModel>();
        }
        public long HotelAgentRoomId { get; set; }
        [Required]
        public int HotelId { get; set; } // dropdown
        [Required]
        public byte RoomId { get; set; }
        [Required]
        public int? NoOfRooms { get; set; }
        [Required]
        public string AgentId { get; set; }
        public int? PriceUnitId { get; set; }
        public double? AgentPrice { get; set; }
        public double? CoustomerPrice { get; set; }
        public bool IsWeekendPrice { get; set; }
        public double? WeekendPagentPrice { get; set; }
        public double? WeekendCoustomerPrice { get; set; }
        public DateTime? EffectiveFrom { get; set; }
        public DateTime? EffectiveTo { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsVerified { get; set; }
        public int? CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }

    public class HotelAgentEditViewModel
    {

        public IEnumerable<HotelDropDownViewModel> HotelDropDownViewModel { get; set; }
        public IEnumerable<PriceUnitDropDownViewModel> PriceUnitDropDownViewModel { get; set; }
        public IEnumerable<RoomTypeDropDownViewModel> RoomTypeDropDownViewModel { get; set; }
        public IEnumerable<AgentDropDownViewModel> AgentDropDownViewModel { get; set; }
        public HotelAgentEditViewModel()
        {
            HotelDropDownViewModel = new List<HotelDropDownViewModel>();
            PriceUnitDropDownViewModel = new List<PriceUnitDropDownViewModel>();
            RoomTypeDropDownViewModel = new List<RoomTypeDropDownViewModel>();
            AgentDropDownViewModel = new List<AgentDropDownViewModel>();
        }

        public long HotelAgentRoomId { get; set; }

        [Required]
        public int HotelId { get; set; } // dropdown
        [Required]
        public byte RoomId { get; set; }
        [Required]
        public int? NoOfRooms { get; set; }
        [Required]
        public string AgentId { get; set; }
        public int? PriceUnitId { get; set; }
        public double? AgentPrice { get; set; }
        public double? CoustomerPrice { get; set; }
        public bool IsWeekendPrice { get; set; }
        public double? WeekendPagentPrice { get; set; }
        public double? WeekendCoustomerPrice { get; set; }
        public DateTime? EffectiveFrom { get; set; }
        public DateTime? EffectiveTo { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsVerified { get; set; }
        public int? CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
    public class RoomTypeDropDownViewModel
    {
        public byte RoomId { get; set; }
        public string RoomType { get; set; }

    }

    public class PriceUnitDropDownViewModel
    {
        public int PriceUnitId { get; set; }
        public string PriceUnitName { get; set; }
    }

    public class HotelDropDownViewModel
    {
        public int HotelId { get; set; }
        public string HotelName { get; set; }
    }

    public class AgentDropDownViewModel
    {
        public int AgentId { get; set; }
        public string AgentName { get; set; }
    }

    public class HotelAgentIndexVIewModel
    {
        public long HotelAgentId { get; set; }
        public string HotelName { get; set; }
        public string HotelAgentName { get; set; }
        public string RoomType { get; set; }
        public double AgentPrice { get; set; }
        public double CustomerPrice { get; set; }
        public string FromDate { get; set; }
        public String ToDate { get; set; }
    }
}