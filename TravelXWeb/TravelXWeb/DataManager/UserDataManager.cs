﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using TravelX.ViewModel;
using TravelXWeb.TravelXDataBaseModel;
using TravelXWeb.ViewModel;

namespace TravelXWeb.DataManager
{
    public class UserDataManager
    {
        public static UserListViewModel GetAllUserForVIew()
        {
            using (TravelXEntities db = new TravelXEntities())
            {
                return ConvertAspNetUserToListViewModel(db.AspNetUsers.Where(usr=>usr.IsDelete==false));
            }
        }

        public static UserCreateViewModel GetUserById(int Id)
        {
            UserCreateViewModel ucvm = new UserCreateViewModel();
            AspNetUser user = new AspNetUser();
            using (TravelXEntities db = new TravelXEntities())
            {
                user = db.AspNetUsers.FirstOrDefault(usr => usr.Id == Id);
                ucvm = ConvertAspUserToUserCreateViewModel(user);
            }
            return ucvm;
        }

        public static UserUpdateViewModel GetUserByIdForUpdate(int Id)
        {
            UserUpdateViewModel ucvm = new UserUpdateViewModel();
            AspNetUser user = new AspNetUser();
            using (TravelXEntities db = new TravelXEntities())
            {
                user = db.AspNetUsers.FirstOrDefault(usr => usr.Id == Id);
                ucvm = ConvertAspUserToUserUpdateViewModel(user);
            }
            return ucvm;
        }

        public static bool DeleteUser(string UserId)
        {
            using (TravelXEntities db = new TravelXEntities())
            {
                var user = db.AspNetUsers.Find(UserId);
                user.IsDelete = true;
                db.AspNetUsers.AddOrUpdate(user);
                db.SaveChanges();
                return true;
            }
        }

        public static UserProfile GetUserProfile(int UserId)
        {
            using (TravelXEntities db = new TravelXEntities())
            {
                var user = db.AspNetUsers.FirstOrDefault(usr => usr.Id == UserId);
                return ConvertUserToProfile(user);
            }
        }

        private static UserProfile ConvertUserToProfile(AspNetUser user)
        {
            return new UserProfile()
            {
                Name = user.FirstName + "" + user.LastName,
                PhoneNO = user.PhoneNumber,
                Email = user.Email
            };
        }
        public static bool UpdateUser(UserUpdateViewModel ucvm)
        {
            AspNetUser usr = ConvertUserCreateViewModelToAspNetUser(ucvm);

            using (TravelXEntities db = new TravelXEntities())
            {
                var roleId = ucvm.RoleId;
                var userId = ucvm.Id;
                db.AspNetUsers.AddOrUpdate(usr);
                db.UpdateUserRole(userId, roleId);
                db.SaveChanges();
                //var userRole = db.AspNetRoles.
            }
            return true;
        }

        public static bool ChnageEmail(string newEmail, string confirmedEmail, string oldEmail)
        {
            using (TravelXEntities db = new TravelXEntities())
            {
                var user = db.AspNetUsers.FirstOrDefault(usr => usr.Email == oldEmail);
                if (user != null)
                {
                    user.Email = newEmail;
                    db.AspNetUsers.AddOrUpdate(user);
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        private static AspNetUser ConvertUserCreateViewModelToAspNetUser(UserUpdateViewModel ucvm)
        {
            if (ucvm.Password != null)
            {
                return new AspNetUser()
                {
                    Id = ucvm.Id,
                    FirstName = ucvm.FirstName,
                    LastName = ucvm.LastName,
                    UserName = ucvm.UserName,
                    Email = ucvm.Email,
                    PhoneNumber = ucvm.PhoneNumber,
                    Address = ucvm.Address,
                    PasswordHash = ucvm.Password,
                    SecurityStamp = ucvm.SecurityStamp,
                    LockoutEnabled = ucvm.LockoutEnabled,
                    CreatedBy = ucvm.CreatedBy,
                    CreatedDate = ucvm.CreatedDate,
                    UpdatedBy = (int)(System.Web.HttpContext.Current.Session["UserId"]),
                    UpdateDate = DateTime.Now,
                    IsActive = ucvm.IsActive,
                    IsDelete = ucvm.IsDelete
                };
            }
            return new AspNetUser()
            {
                Id = ucvm.Id,
                FirstName = ucvm.FirstName,
                LastName = ucvm.LastName,
                UserName = ucvm.UserName,
                Email = ucvm.Email,
                PhoneNumber = ucvm.PhoneNumber,
                Address = ucvm.Address
            };
        }

        private static UserListViewModel ConvertAspNetUserToListViewModel(IEnumerable<AspNetUser> users)
        {
            UserListViewModel vm = new UserListViewModel();
            foreach (AspNetUser usr in users)
            {
                vm.UserList.Add(ConvertUserToViewModel(usr));
            }
            return vm;
        }

        private static UserCreateViewModel ConvertAspUserToUserCreateViewModel(AspNetUser user)
        {
            return new UserCreateViewModel()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                Email = user.Email,
                Address = user.Address,
                PhoneNumber = user.PhoneNumber,
                RoleId = user.AspNetRoles.FirstOrDefault().Id
            };
        }
        private static UserUpdateViewModel ConvertAspUserToUserUpdateViewModel(AspNetUser user)
        {
            return new UserUpdateViewModel()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                Email = user.Email,
                Address = user.Address,
                PhoneNumber = user.PhoneNumber,
                RoleId = user.AspNetRoles.FirstOrDefault().Id,
                Password = user.PasswordHash,
                SecurityStamp = user.SecurityStamp,
                LockoutEnabled = user.LockoutEnabled,
                CreatedBy = user.CreatedBy,
                CreatedDate = user.CreatedDate,
                UpdatedBy = user.UpdatedBy,
                UpdateDate = user.UpdateDate,
                IsActive = user.IsActive,
                IsDelete = user.IsDelete
            };
        }
        private static UserViewModel ConvertUserToViewModel(AspNetUser user)
        {
            return new UserViewModel()
            {
                Id = user.Id,
                Email = user.Email,
                // UserName = user.UserName,
                Name = user.FirstName + " " + user.LastName,
                RoleName = user.AspNetRoles.FirstOrDefault().Name,
                Phone = user.PhoneNumber
            };
        }
    }

}