﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using TravelXWeb.Models;

namespace TravelXWeb.Controllers
{
    public class BaseController : Controller
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }
        protected override void Initialize(RequestContext requestContext)
        {

            base.Initialize(requestContext);
            if (Session["UserId"] == null || ReferenceEquals(Session["UserId"], string.Empty))
            {
                SetLoggedInUserProperties();                
            }
        }       

        private void SetLoggedInUserProperties()
        {
            ApplicationUser currentUser = UserManager.FindByName(User.Identity.Name);
            if (currentUser != null && currentUser.Roles.Any())
            {
                Session["UserId"] = currentUser.Id;
                Session["UserFirstLastName"] = currentUser.FirstName + " " + currentUser.LastName;
                var roles = currentUser.Roles.ToList();
                var identityUserRole = roles.FirstOrDefault();
                if (identityUserRole != null)
                {
                    Session["UserRoleId"] = identityUserRole.RoleId;
                }
            }
        }
    }
}