﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelX.ViewModel
{
    public class SuccessErrorModel
    {
        public bool IsSuccessful { get; set; }
        public string ErrorMessage { get; set; }
        public Exception ExceptionObject { get; set; }

        public int id { get; set; }

        public SuccessErrorModel(bool success, string message = null, Exception ex = null, int id = 0)
        {
            this.IsSuccessful = success;
            this.ErrorMessage = message;
            this.ExceptionObject = ex;
            this.id = id;
        }
        //EG-1091 12/15/2016 M.Waris Javed
        public SuccessErrorModel() { }
    }
}