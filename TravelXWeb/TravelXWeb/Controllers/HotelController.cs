﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TravelXWeb.DataManager;
using TravelXWeb.ViewModel;

namespace TravelXWeb.Controllers
{
    [Authorize]
    public class HotelController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: Hotel
        public ActionResult Index()
        {
            HotelIndexViewModelList vm = HotelDataManager.GetAllHotelForIndexView();
            return View(vm);
        }

        // GET: Hotel/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Hotel/Create
        public ActionResult Create()
        {
            try
            {
                HotelCreateViewModel viewModel = new HotelCreateViewModel();
                viewModel = HotelDataManager.GetHotelCreateViewModel();
                return View(viewModel);
            }
            catch (Exception ex)
            {
                logger.Error("User Create Page Load", ex);
                return View("Error");
            }
        }

        // POST: Hotel/Create
        [HttpPost]
        public ActionResult Create(HotelCreateViewModel viewModel)
        {
            
            // TODO: Add insert logic here
            var result = HotelDataManager.HotelCreateViewModel(viewModel);
            if (result.IsSuccessful)
            {
                return RedirectToAction("Index");
            }
            else
            {
                //return ModelState.AddModelError("HotelAgent", result.ExceptionObject != null ? result.ExceptionObject.ToString() : result.ErrorMessage);
                return View("Error");
            }
        }

        // GET: Hotel/Edit/5
        public ActionResult Edit(int id)
        {
            HotelIndexEditViweModel vm = new HotelIndexEditViweModel();
            vm=HotelDataManager.GetHotelForEdit(id);
            ViewBag.Latittude = vm.Latitude;
            ViewBag.Longitude = vm.Longitude;
            return View(vm);
        }

        // POST: Hotel/Edit/5
        [HttpPost]
        public ActionResult Edit(HotelIndexEditViweModel viewModel)
        {         
            // TODO: Add insert logic here
            var result = HotelDataManager.HotelEdit(viewModel);
            if (result.IsSuccessful)
            {
                return RedirectToAction("Index");
            }
            else
            {
                //return ModelState.AddModelError("HotelAgent", result.ExceptionObject != null ? result.ExceptionObject.ToString() : result.ErrorMessage);
                return View("Error");
            }
        }

        // GET: Hotel/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Hotel/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult HotelImageDelete(int hotelImageId)
        {
            var result = HotelDataManager.HotelImageDelete(hotelImageId);
            return Json(result,JsonRequestBehavior.AllowGet);
        }
    }
}
