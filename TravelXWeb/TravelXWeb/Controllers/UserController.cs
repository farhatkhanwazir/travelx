﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using TravelXWeb.DataManager;
using TravelXWeb.Models;
using TravelX.ViewModel;
using TravelXWeb.TravelXDataBaseModel;
using TravelXWeb.ViewModel;


namespace TravelXWeb.Controllers
{
    //[Authorize(Roles = "SystemAdministrator, AccountAdministrator, VenueAdministrator")]
    public class UserController : BaseController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;

        public UserController()
        {
        }
        public UserController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        TravelXEntities db = new TravelXEntities();
        // GET: User
        public ActionResult Index(int? page)
        {
            //var userViewModelList = from user in db.AspNetUsers select user;
            var userViewModelList = UserDataManager.GetAllUserForVIew();
            //if (!String.IsNullOrEmpty(searchString))
            //{
            //    webUsers = userViewModelList.Where(l => l.FirstName.Contains(searchString) || l.LastName.Contains(searchString));
            //}
            int pageSize = 5;
            int pageNumber = (page.HasValue ? page.Value : 1);
            ViewBag.PageNumber = pageNumber;
            ViewBag.PageSize = pageSize;
            return View(userViewModelList);

            //return View();
        }

        public ActionResult DashBoardSummery()
        {
            return View();
        }

        public ActionResult UserProfile()
        {
            int userId = (int)(System.Web.HttpContext.Current.Session["UserId"]);
            UserProfile uerProfile = UserDataManager.GetUserProfile(userId);                
            return View(uerProfile);
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(UserLoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);
            //ApplicationUser currentUser = UserManager.FindByName(User.Identity.Name);
            //if (currentUser != null && currentUser.Roles.Any())
            //{
            //    Session["UserId"] = currentUser.Id;
            //    Session["UserFirstLastName"] = currentUser.FirstName + " " + currentUser.LastName;
            //    var roles = currentUser.Roles.ToList();
            //    var identityUserRole = roles.FirstOrDefault();
            //    if (identityUserRole != null)
            //    {
            //        Session["UserRoleId"] = identityUserRole.RoleId;
            //    }
            //    //return RedirectToLocal(returnUrl);
            //    return RedirectToAction("Index");
            //}
            //ModelState.AddModelError("", "Invalid login attempt.");
            //return View(model);
            //ApplicationUser currentUser = UserManager.FindByName(User.Identity.Name);

            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToAction("DashBoardSummery"); ;
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: User/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        [Authorize]
        //GET: User/Create
        public ActionResult Create()
        {
            try
            {
                //var currUser = UserManager.FindById(User.Identity.GetUserId());
                UserCreateViewModel ucvm = new UserCreateViewModel();
                ucvm.systemRole = db.AspNetRoles.ToList();
                return View(ucvm);
            }
            catch (Exception ex)
            {
                logger.Error("User Create Page Load", ex);
                return View("Error");
            }

        }

        // POST: User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UserCreateViewModel userViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // TODO: Add insert logic here
                    var user = new ApplicationUser {FirstName = userViewModel.FirstName, LastName = userViewModel.LastName, UserName = userViewModel.UserName, Email = userViewModel.Email, PhoneNumber = userViewModel.PhoneNumber, Address = userViewModel.Address,IsActive = true,IsDelete = false,CreatedBy = (int)(System.Web.HttpContext.Current.Session["UserId"]), CreatedDate = DateTime.Now,UpdatedBy = default(int),UpdateDate = null};
                    //var user = new ApplicationUser {FirstName = userViewModel.FirstName, LastName = userViewModel.LastName, UserName = userViewModel.UserName, Email = userViewModel.Email, PhoneNumber = userViewModel.PhoneNumber, Address = userViewModel.Address,IsActive = true,IsDelete = false,CreatedBy = 1, CreatedDate = DateTime.Now,UpdatedBy = default(int),UpdateDate = null};
                    user.Roles.Add(new CustomUserRole() { UserId = user.Id, RoleId = userViewModel.RoleId});                    
                    var result = await UserManager.CreateAsync(user, userViewModel.Password);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index");                      
                    }
                    string errors = ((string[])result.Errors)[0];
                    ModelState.AddModelError("", errors);
                    //AddErrors(result);
                    return View(userViewModel);
                }
                return View(userViewModel);
            }
            catch (Exception ex)
            {
                logger.Error("Create User Error", ex);
                return View("Error");
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(int Id)
        {

            UserUpdateViewModel ucvm = new UserUpdateViewModel();
            ucvm = UserDataManager.GetUserByIdForUpdate(Id);
            ucvm.systemRole = db.AspNetRoles.ToList();
            return View(ucvm);
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(UserUpdateViewModel ucvm)
        {
            try
            {
                // TODO: Add update logic here
                bool result = UserDataManager.UpdateUser(ucvm);
                if (result == true)
                {
                    return RedirectToAction("Index");
                }
                return View(ucvm);
            }
            catch (Exception ex)
            {
                logger.Error("Edit User Error", ex);
                return View("Error");
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(string id)
        {
            try
            {
                bool result = UserDataManager.DeleteUser(id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                logger.Error("Edit User Error", ex);
                return View("Error");
            }
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult UserSetting()
        {
            UserSetting us = new UserSetting();
            return View(us);
        }        
        public async Task<ActionResult> UserSettingPassword(string newPassword , string confirmNewPassword, string oldPassword )
        {
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId<int>(), oldPassword, newPassword);
            if (result.Succeeded)
            {
            return Json(true,JsonRequestBehavior.AllowGet);
            }
            //bool result = UserDataManager.SettingPassword(newPassword,confirmedPassword,oldPassword);
            return Json(false,JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserSettingEmail(string newEmail,string confirmEmail, string oldEmail)
        {
            bool result= UserDataManager.ChnageEmail(newEmail, confirmEmail, oldEmail);
            if (result == true)
            {
            return Json(true,JsonRequestBehavior.AllowGet);
            }
            return Json(false,JsonRequestBehavior.AllowGet);
        }       
        //#region Overrides

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }
    }
}
